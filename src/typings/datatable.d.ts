import Sequelize from 'sequelize';

export type DatatableFunction = (
    model: Sequelize.Model<any, any>,
    config: object,
    modelParams: {
        attributes?: Array<string>,
        where?: any
    }, 
    options?: object
) => any;
