import * as Sequelize from "sequelize";
import {UserInstance, UserAttributes} from '../db/models/user.model';
import {CodeInstance, CodeAttributes} from '../db/models/code.model';

export interface DbInterface {
    sequelize: Sequelize.Sequelize;
    Sequelize: Sequelize.SequelizeStatic;
    environment: string;
    User: Sequelize.Model<UserInstance, UserAttributes>;  
    Code: Sequelize.Model<CodeInstance, CodeAttributes>;  
}

