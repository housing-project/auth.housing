import fastify from 'fastify';
import { Server, IncomingMessage, ServerResponse } from 'http';
import fastifyFormbody from 'fastify-formbody';
import dotenv from 'dotenv';
import helmet from 'fastify-helmet';
import fastswagger from 'fastify-swagger';
import http from 'http';

dotenv.config({ path: __dirname + '/../.env' });
import dbConnector from './db/plugins/dbConnector';
import routes from './routes';

const app: fastify.FastifyInstance<Server, IncomingMessage, ServerResponse> = fastify(
    {
        logger: true,
        trustProxy: true
    }
);
app.register(helmet);
app.register(dbConnector);
app.register(fastifyFormbody);

const configs = (require('../config/url.json')[process.env.NODE_ENV]);


// SWAGGER
const host = process.env.HOST || configs.host;
const scheme = process.env.SCHEME || configs.scheme;
const expose = process.env.NODE_ENV === 'production' ? false : true;

app.register(fastswagger, {
    exposeRoute: 1 ? true : false,
    routePrefix: '/',
    swagger: {
        info: {
            title: 'AUTH SERVICE',
            description: 'site auth service',
            version: '1.0.0'
        },
        host: configs.host+":"+configs.port,
        schemes: [configs.scheme],
        consumes: ['application/json'],
        produces: ['application/json'],
        tags: [],
        securityDefinitions: {
            "Authorization": {
                "type": "apiKey",
                "name": "Authorization",
                "in": "header"
            }
        }
    }
});

routes(app);

app.listen(configs.port, '0.0.0.0', (err, address) => {
    if(err){
        app.log.error(err);
        process.exit(1);
    }
    app.log.info(`Server listening on ${address}`);
});

module.exports = app;