import * as fastify from 'fastify';
import userRoute from './v1/user';
import authRoute from './v1/auth';
import resetRoute from './v1/reset';

export default (app: fastify.FastifyInstance) => {

    app.register(userRoute, { prefix: '/api/v1/user' });
    app.register(authRoute, { prefix: '/api/v1/user' });
    app.register(resetRoute, { prefix: '/api/v1/user' });
}