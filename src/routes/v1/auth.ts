import * as fastify from 'fastify';
import Responses from '../../utils/responses';
// require('../../models/user.model')();
import {RouteSchema} from '../../@types/routeschema';
import AuthController from '../../controllers/auth.controller';
import Requests from '../../utils/requests';

export default (app: fastify.FastifyInstance, options, done) => {
    // const db = app['cockroachdb'].db;
    // const datatable = app['cockroachdb'].dt;
    const action = 'Auth';

    const userInterface = {
        email: {type: 'string'},
        password: {type: 'string'},
    };

    /**
     * USER LOGIN AUTH ENDPOINT HERE
     */
    const createUserSchema: RouteSchema = {
        tags: [`${action}`], summary: `Login to app`,
        body: Requests.requestBody(userInterface, ['email', 'password']),
        response: {
            400: Responses.errorResponseObj(),
            200: Responses.successResponseObj({ 
                type: 'object',
                properties: {
                    id: { type: 'string' }, ...userInterface, created_at: { type: 'string' }, updated_at: { type: 'string' }
                }
            })
        }
    }
    app.post('/auth', { schema: createUserSchema, attachValidation: true }, AuthController.loginUser);

        /**
     * USER LOGIN AUTH ENDPOINT HERE
     */
    const ValidateTokenSchema: RouteSchema = {
        tags: [`${action}`], summary: `Authenticate user`,
        response: {
            400: Responses.errorResponseObj(),
            200: Responses.successResponseObj({ 
                type: 'object',
                properties: {
                    id: { type: 'string' }, ...userInterface, created_at: { type: 'string' }, updated_at: { type: 'string' }
                }
            })
        }
    }
    app.post('/auth-token', { schema: ValidateTokenSchema, attachValidation: true }, AuthController.validateToken);

    done();
};