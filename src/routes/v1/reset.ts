import * as fastify from 'fastify';
import Responses from '../../utils/responses';
// require('../../models/user.model')();
import {RouteSchema} from '../../@types/routeschema';
import ResetController from '../../controllers/reset.controller';
import Requests from '../../utils/requests';

export default (app: fastify.FastifyInstance, options, done) => {
    // const db = app['cockroachdb'].db;
    // const datatable = app['cockroachdb'].dt;
    const action = 'Auth';

    const userInterface = {
        email: {type: 'string'}  
    };

    /**
     * USER REGISTRATION AUTH ENDPOINT HERE
     */
    const resetUserPassworCodedSchema: RouteSchema = {
        tags: [`${action}`], summary: ` Send reset link endpoint`,
        body: Requests.requestBody(userInterface, ['email']),
        response: {
            400: Responses.errorResponseObj(),
            200: Responses.successResponseObj({ 
                type: 'object',
                properties: {
                    id: { type: 'string' }, ...userInterface, created_at: { type: 'string' }, updated_at: { type: 'string' }
                }
            })
        }
    }
    app.post('/send-reset-link', { schema: resetUserPassworCodedSchema, attachValidation: true }, ResetController.emailReset);


    const resetUserPasswordSchema: RouteSchema = {
        tags: [`${action}`], summary: ` Password reset endpoint`,
        body: Requests.requestBody({ reset_key: {type: 'string'}, password: {type: 'string'}  }, ['reset_key', 'password']),
        response: {
            400: Responses.errorResponseObj(),
            200: Responses.successResponseObj({ 
                type: 'object',
                properties: {
                    id: { type: 'string' }, ...userInterface, created_at: { type: 'string' }, updated_at: { type: 'string' }
                }
            })
        }
    }
    app.post('/reset', { schema: resetUserPasswordSchema, attachValidation: true }, ResetController.passwordReset);

    const changeUserPasswordSchema: RouteSchema = {
        tags: [`${action}`], summary: ` Password reset endpoint`,
        params: Requests.requestParams({ id: { type: 'string', description: 'User id'} }),
        body: Requests.requestBody({ password: {type: 'string'}  }, ['password']),
        response: {
            400: Responses.errorResponseObj(),
            200: Responses.successResponseObj({ 
                type: 'object',
                properties: {
                    id: { type: 'string' }, ...userInterface, created_at: { type: 'string' }, updated_at: { type: 'string' }
                }
            })
        }
    }
    app.patch('/change-password/:id', { schema: changeUserPasswordSchema, attachValidation: true }, ResetController.changePassword);

    done();
};