import  * as fastify from 'fastify';
import {db} from '../db/sequelize';
import validator from '../utils/requestValidator';
import Responses from '../utils/responses';
import bcrypt from 'bcrypt';
// // import {logger, head} from '../libraries/tracer/logger';
import { ServerResponse } from "http";

class UserController{

    static async createUser(request: fastify.FastifyRequest, response: fastify.FastifyReply<ServerResponse>){
        const db = request.cockroachdb.db;        
        const {error, errorData} = validator(request);
        if(error) return response.code(400).send(errorData);

        try {
            let user = await db.User.findOne({ where: { email: request.body.email }, raw: true });
            if (user) {
                return response.send( Responses.errorResponses('001', 'Email unavailable', []) )
            };
            
            user = await db.User.findOne({ where: { user_name: request.body.user_name } });
            if(user) {
                return response.send( Responses.errorResponses('001', 'Username unavailable', []) )
            };

            const salt = await bcrypt.genSalt(10);
            const hashedPassword = await bcrypt.hash(request.body.password, salt);
            
            user = await db.User.create({
                email: request.body.email,
                user_name: request.body.user_name,
                password: hashedPassword,              
            });
            return response.send(Responses.successResponses('001', 'User created', [] ));
        } catch (error) {
            console.log(error);
            
            return response.send( Responses.errorResponses('001', 'invalid request', []) )
        }
        
    };
};

export default UserController;