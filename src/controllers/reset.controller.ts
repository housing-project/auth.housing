import  * as fastify from 'fastify';
import {db} from '../db/sequelize';
import validator from '../utils/requestValidator';
import Responses from '../utils/responses';
import bcrypt from 'bcrypt';
import nodemailer from "nodemailer";
import requestmaker from '../utils/reqmaker'

// // import {logger, head} from '../libraries/tracer/logger';
import { ServerResponse } from "http";

class ResetController{

    static async emailReset(request: fastify.FastifyRequest, response: fastify.FastifyReply<ServerResponse>){
        const db = request.cockroachdb.db;
        const {error, errorData} = validator(request);
        if(error) return response.code(400).send(errorData);

        try {
            console.log(request.body.email);
            
            let user = await db.User.findOne({ where: { email: request.body.email }, raw: true });
            
            if (!user) {
                return response.status(400).send(Responses.errorResponses( '001', 'Email unavailable', [] ))
            };
            const reset_key = `${Date.now()}`;
         
            // send email
            let email;
            try {
                email = await requestmaker({
                    service: 'notification',
                    action: 'email/send-templated',
                    method: 'POST',
                    data: {
                        user_id: user.id,
                        details: {
                            user_name: user.user_name,
                            to:  request.body.email,
                            reset_key: reset_key
                        }
                    },
                })

                await db.Code.create({ user_id: user.id, reset_key: reset_key })

            } catch (ex) {
                return response.code(400).send(ex)
            }
            response.send(email);

            return response.send(Responses.successResponses( '001', 'Email sent', [] ));
        } catch (error) {
            return response.send( Responses.errorResponses('001', 'error', 'invalid request') )
        }
    };

    static async passwordReset(request: fastify.FastifyRequest, response: fastify.FastifyReply<ServerResponse>){
        const db = request.cockroachdb.db;
        const {error, errorData} = validator(request);
        if(error) return response.code(400).send(errorData);

        const findcode = await db.Code.findOne({ where: { reset_key: request.body.reset_key }, raw: true });
        if(!findcode) {
            return response.send( Responses.errorResponses('001', 'Sorry wrong reset code', []) )
        }

        console.log(findcode);
        const finduser = await db.User.findOne({ where: { id: findcode.user_id } })
        
        if(!finduser) {
            return response.send( Responses.errorResponses('001', 'Sorry no user available', []) )
        }

        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(request.body.password, salt);
        
        await finduser.update({ password: hashedPassword });

        // await db.Code.destroy({ where: { user_id: findcode.id }, force: true  })
        await db.Code.destroy({ where: { reset_key: findcode.reset_key }, force: true })

        return response.send(Responses.successResponses('001', 'User password updated', [] ));
    };

    static async changePassword(request: fastify.FastifyRequest, response: fastify.FastifyReply<ServerResponse>){
        const db = request.cockroachdb.db;
        const {error, errorData} = validator(request);
        if(error) return response.code(400).send(errorData);

        console.log(request.params);
        
        const finduser = await db.User.findOne({ where: { id: request.params.id } })
        
        if(!finduser) {
            return response.send( Responses.errorResponses('001', 'Sorry no user available', []) )
        }

        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(request.body.password, salt);
        
        await finduser.update({ password: hashedPassword });

        return response.send(Responses.successResponses('001', 'User password changed', [] ));
    };
};

export default ResetController;