import  * as fastify from 'fastify';
import {db} from '../db/sequelize';
import validator from '../utils/requestValidator';
import Responses from '../utils/responses';
import jwt from 'jsonwebtoken';
import { Op } from'sequelize';
import bcrypt from 'bcrypt';
// // import {logger, head} from '../libraries/tracer/logger';
import { ServerResponse } from "http";

class AuthController{
    
    static async loginUser(request: fastify.FastifyRequest, response: fastify.FastifyReply<ServerResponse>){
        const db = request.cockroachdb.db;
        const {error, errorData} = validator(request);
        if(error) return response.code(400).send(errorData);
        // const db = request.cockroachdb.db;
        // const span = head(request, 'USER AUTHENTICATION: Login user');
        
        try {
            let validEmail = await db.User.findOne({ where: { [Op.or]: [{user_name: request.body.email}, {email: request.body.email}] } });
            if (!validEmail) {
                return response.status(400).send(Responses.errorResponses( '001', 'Invalid credentials', [] ))
            };
            
            const validpassword = await bcrypt.compare(request.body.password, validEmail.password);
            if(!validpassword) {
                return response.status(400).send(Responses.errorResponses( '001', 'Invalid credentials',  [] ))
            };
            
            const token = jwt.sign({
                id: validEmail.id,
                email: validEmail.email,
                user_name: validEmail.user_name                         
            }, `${process.env.PRIVATE_KEY}`);

            return response.send(token);
        } catch (error) {
            return response.send( Responses.errorResponses('001', 'error', []) )
        }
    };

    static async validateToken(request: fastify.FastifyRequest, response: fastify.FastifyReply<ServerResponse>){
        const db = request.cockroachdb.db;
        const token = request.headers["authorization"];        
        if (!token) return response.status(400).send('Access denied');

        try {
            const decoded = jwt.verify(token, `${process.env.PRIVATE_KEY}`);
            console.log(decoded);
            // // found = decoded;
            return response.status(200).send(decoded.id);

        } catch (error) {
            response.status(400).send('Invalid token');
        }

    }

};

export default AuthController;