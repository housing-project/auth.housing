export const datatableObj = {
    draw: { type: 'integer' },
    columns: { 
        type: 'array', 
        items: { 
            type: 'object',
            additionalProperties: false,
            properties: {
                data: { type: 'string' },
                name: { type: 'string' },
                searchable: { type: 'boolean' },
                orderable: { type: 'boolean' },
                search: { 
                    type: 'object',
                    properties: {
                        value: { type: 'string' },
                        regex: { type: 'boolean' }
                    }
                }
            }
        } 
    },
    order: {
        type: 'array', 
        items: { 
            type: 'object',
            properties: {
                column: { type: 'integer' },
                dir: {
                    type: 'string',
                    enum: ['asc', 'desc']
                  }
            }
        }
    },
    start: { type: 'integer' },
    length: { type: 'integer' },
    search: { 
        type: 'object',
        properties: {
            value: { type: 'string' },
            regex: { type: 'boolean' }
        }
    }
}