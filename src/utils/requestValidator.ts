import Responses from './responses';
const validator = (req) => {
    if (req['validationError']) {
        const errors = req['validationError'].validation.map( mes => {
            return { code: '000', description: mes.message } 
        });
        return {
            error: true,
            errorData: Responses.errorResponses('001', 'Invalid credentials', errors)
        }
    } else {
        return {
            error: false,
            errorData: ''
        }
    }
}
export default validator;