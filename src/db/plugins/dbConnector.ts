import fastify from 'fastify';
import fastifyPlugin from 'fastify-plugin';
import { db } from '../sequelize';
import datatable from 'sequelize-datatable';
// import {createViews} from '../db/views';
import {DatatableFunction} from '../../typings/datatable';
import {createViews} from '../../db/views';
import { DbInterface } from '../../typings/dbInterface';


const datatable: DatatableFunction = require('sequelize-datatable')

declare module 'fastify' {
    interface FastifyRequest {
        cockroachdb: {db: DbInterface,
            dt: DatatableFunction}
    }
}

async function dbConnector(app: fastify.FastifyInstance, options){
    
    createViews(db);
    
    const data = {
        db: db,
        dt: datatable
    }  
    app.decorateRequest('cockroachdb', data);
}

export default fastifyPlugin(dbConnector);