import {
    QueryInterface,
    SequelizeStatic
} from 'sequelize';

export = {
    up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
        return queryInterface.createTable('Users', {
            id: {
                type: Sequelize.UUID, 
                defaultValue: Sequelize.UUIDV4, 
                primaryKey: true
            },

            email: {
                type: Sequelize.STRING
            },

            user_name: {
                type: Sequelize.STRING
            },

            password: {
                type: Sequelize.STRING
            },

            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
                field: 'created_at'
            },

            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
                field: 'updated_at'
            },

            deletedAt: {
                type: Sequelize.DATE, 
                field: 'deleted_at'
            }
        });
    },

    down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
        return queryInterface.dropTable('Users');
    }
};
