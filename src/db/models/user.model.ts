import * as Sequelize from 'sequelize';
import { SequelizeAttributes } from '../../typings/SequelizeAttributes';

export interface UserAttributes {
    id?: string;

    email ? : string;
    user_name ? : string;
    password ? : string;  

    created_at?: Date;
    updated_at?: Date;
    deleted_at?: Date;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
}

export interface UserInstance extends Sequelize.Instance<UserAttributes>, UserAttributes {
}

export const UserFactory = (sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes): Sequelize.Model<UserInstance, UserAttributes> => {
    
    const attributes: SequelizeAttributes<UserAttributes> = {
        id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV1, primaryKey: true },

        email: DataTypes.STRING,
        user_name: DataTypes.STRING,
        password: DataTypes.STRING,    
                
        createdAt: {type: DataTypes.DATE, field: 'created_at'},
        updatedAt: {type: DataTypes.DATE, field: 'updated_at'},
        deletedAt: {type: DataTypes.DATE, field: 'deleted_at'}
    } 
    
    var User = sequelize.define<UserInstance, UserAttributes>('User', attributes, {
        timestamps: true,
        paranoid: true
    });

    User.associate = (models) => {
        // User.belongsTo(models.Product, { foreignKey: 'product_id' });  
    }

    return User;
};