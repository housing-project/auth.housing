import * as Sequelize from 'sequelize';
import { SequelizeAttributes } from '../../typings/SequelizeAttributes';

export interface CodeAttributes {
    id?: string;

    reset_key ? : string;

    user_id ? : string;   

    created_at?: Date;
    updated_at?: Date;
    deleted_at?: Date;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
}

export interface CodeInstance extends Sequelize.Instance<CodeAttributes>, CodeAttributes {
}

export const CodeFactory = (sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes): Sequelize.Model<CodeInstance, CodeAttributes> => {
    
    const attributes: SequelizeAttributes<CodeAttributes> = {
        id: {  type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4, primaryKey: true },

        user_id: DataTypes.STRING,
        reset_key: DataTypes.STRING,
                
        createdAt: {type: DataTypes.DATE, field: 'created_at'},
        updatedAt: {type: DataTypes.DATE, field: 'updated_at'},
        deletedAt: {type: DataTypes.DATE, field: 'deleted_at'}
    } 
    
    var Code = sequelize.define<CodeInstance, CodeAttributes>('Code', attributes, {
        timestamps: true,
        paranoid: true
    });

    Code.associate = (models) => {
        // Code.belongsTo(models.Product, { foreignKey: 'product_id' });  
    }

    return Code;
};