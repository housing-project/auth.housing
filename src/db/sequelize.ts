require('dotenv').config();
import {Sequelize} from 'sequelize';
import * as dotenv from 'dotenv';
import * as fs from 'fs';
import { UserFactory } from './models/user.model';
import { CodeFactory } from './models/code.model';
import { DbInterface } from '../typings/dbInterface';

dotenv.config({path: __dirname + '/../../.env'});
const dbData = require('../../config/db.js')[process.env.NODE_ENV];
let sequelize = new Sequelize(dbData);

export const db: DbInterface = {
    sequelize,
    Sequelize,
    environment: process.env.NODE_ENV,
    User: UserFactory( sequelize, Sequelize),
    Code: CodeFactory( sequelize, Sequelize),
}

// Object.keys(db).forEach(modelName => {
//     if (db[modelName].associate) {
//         db[modelName].associate(db);
//     }
// });

