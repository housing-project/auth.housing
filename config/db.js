const dotenv = require('dotenv');
const fs = require('fs');
dotenv.config({ path: __dirname + '/.env' });

if(process.env.NODE_ENV === 'test'){
  const host = process.env.DB_HOST || 'localhost';

  module.exports = {
    test:{
        username: "postgres",
        password: "postgres",
        database: "authdb",
        dialect: "postgres",
        host: "127.0.0.1",
        logging: false,
        operatorsAliases: false,
        typeValidation: true,
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        storage: ':memory:'
    }
  }
} else{
  module.exports = {
    production: {
      username: "animey",
        database: "authdb",
        password: "jqkwngpzru6bphez",
        dialect: "postgres",
        host: "db-postgresql-nyc1-57292-do-user-8528190-0.b.db.ondigitalocean.com",
        port: 25060,
        logging: true,
        operatorsAliases: false,
        dialectOptions: {
          ssl:{
            require: true, 
            rejectUnauthorized: false 
          }
        },
        pool: {
          max: 5,
          min: 0,
          idle: 10000
      },
    },  

  }
}